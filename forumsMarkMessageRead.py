#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
elRepo.io

Copyright (C) 2020  Gioacchino Mazzurco <gio@eigenlab.org>
Copyright (C) 2020  Asociación Civil Altermundi <info@altermundi.net>

SPDX-License-Identifier: AGPL-3.0-only
"""

import retroshareJsonApi as rs
import sys

def setMessageRead(forumId, messageId, read = True):
	return rs.jsonApiCall( "/rsGxsForums/markRead",
					{ "messageId":
						{
							"first": forumId,
							"second": messageId
						},
						"read": read
					} )["retval"]

def main():
	print(setMessageRead(sys.argv[1], sys.argv[2]))

if __name__ == '__main__':
	main()
