#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
elRepo.io

Copyright (C) 2020  Gioacchino Mazzurco <gio@eigenlab.org>
Copyright (C) 2020  Asociación Civil Altermundi <info@altermundi.net>

SPDX-License-Identifier: AGPL-3.0-only
"""

import retroshareJsonApi as rs


def logEvents(eventType = rs.RsEventType___NONE):
	for event in rs.getEventsStream(eventType):
		print(event)

def main():
	import sys
	eventType = rs.RsEventType___NONE
	if len(sys.argv) == 2:
		try:
			eventType = int(sys.argv[1])
		except:
			print("Invalid argument: ", sys.argv[1])
			return
	logEvents(eventType)

if __name__ == '__main__':
	main()
