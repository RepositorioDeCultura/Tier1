#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
elRepo.io

Copyright (C) 2021  Gioacchino Mazzurco <gio@eigenlab.org>
Copyright (C) 2021  Asociación Civil Altermundi <info@altermundi.net>

SPDX-License-Identifier: AGPL-3.0-only
"""
from rspeers import *

def main():
	print(
		getRetroshareInvite(
			inviteFlags =
			RetroshareInviteFlags.FULL_IP_HISTORY.value | RetroshareInviteFlags.DNS.value ) )

if __name__ == '__main__':
    main()
