#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
elRepo.io

Copyright (C) 2020  Gioacchino Mazzurco <gio@eigenlab.org>
Copyright (C) 2020  Asociación Civil Altermundi <info@altermundi.net>

SPDX-License-Identifier: AGPL-3.0-only
"""
import retroshareJsonApi as rs

RS_FILE_HINTS_DOWNLOAD = 0x00000010
RS_FILE_HINTS_UPLOAD   = 0x00000020

# FileInfo: i
def printFileInfo(i):
	print(
			i["hash"],
			str(int(100*i["avail"]["xint64"]/i["size"]["xint64"]))+"%",
			str(int(i["tfRate"])) + "KB/s", i["fname"] )

def main():
	for mHash in rs.jsonApiCall("/rsFiles/FileDownloads")["hashs"]:
		printFileInfo(rs.jsonApiCall(
			"/rsFiles/FileDetails",
			{"hash": mHash, "hintflags": RS_FILE_HINTS_DOWNLOAD} )["info"])

if __name__ == '__main__':
    main()
