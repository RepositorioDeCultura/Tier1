#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
elRepo.io

Copyright (C) 2020-2021  Gioacchino Mazzurco <gio@eigenlab.org>
Copyright (C) 2020-2021  Asociación Civil Altermundi <info@altermundi.net>

SPDX-License-Identifier: AGPL-3.0-only
"""

from enum import Enum
import retroshareJsonApi as rs

class RsForumEventCode(Enum):
	UNKNOWN                  = 0x00,
	NEW_FORUM                = 0x01,
	UPDATED_FORUM            = 0x02,
	NEW_MESSAGE              = 0x03,
	UPDATED_MESSAGE          = 0x04,
	SUBSCRIBE_STATUS_CHANGED = 0x05,
	READ_STATUS_CHANGED      = 0x06,
	STATISTICS_CHANGED       = 0x07,
	MODERATOR_LIST_CHANGED   = 0x08,
	SYNC_PARAMETERS_UPDATED  = 0x0a,
	PINNED_POSTS_CHANGED     = 0x0b,
	DELETED_FORUM            = 0x0c

""" You have the admin key for this group """
GXS_GROUP_SUBSCRIBE_ADMIN = 0x01

"""
You have the publish key for thiss group. Typical use: publish key in
channels are shared with specific friends.
"""
GXS_GROUP_SUBSCRIBE_PUBLISH = 0x02

"""
you are subscribed to a group, which makes you a source for this group to your
friend nodes.
"""
GXS_GROUP_SUBSCRIBE_SUBSCRIBED = 0x04

"""You are not subscribed"""
GXS_GROUP_SUBSCRIBE_NOT_SUBSCRIBED = 0x08

# RsGroupMetaData f
def printForumSummary(f):
	print(f["mGroupId"], f["mGroupName"], f["mAuthorId"], f["mPublishTs"]["xint64"])

def getForumsSummaries():
	return rs.jsonApiCall(
		"/rsGxsForums/getForumsSummaries")["forums"]

def createForum(name, description):
	return rs.jsonApiCall(
		"/rsGxsForums/createForumV2",
		{
			"name": name,
			"description": description
		} )

### DEPRECATED

"""New forum has been received"""
RsForumEventCode_NEW_FORUM = 0x01

"""New message reeived in a particular forum"""
RsForumEventCode_NEW_MESSAGE = 0x03
