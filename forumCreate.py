#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
elRepo.io

Copyright (C) 2021  Gioacchino Mazzurco <gio@eigenlab.org>
Copyright (C) 2021  Asociación Civil Altermundi <info@altermundi.net>

SPDX-License-Identifier: AGPL-3.0-only
"""

import sys
from rsgxsforums import createForum
from rslog import err

def main():
	result = createForum(sys.argv[1], sys.argv[2])

	if(result["retval"]):
		print(result["forumId"])
		return 0
	
	err(result["errorMessage"])
	return -2

if __name__ == '__main__':
	main()
