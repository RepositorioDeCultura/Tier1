/*
 * elRepo.io
 * 
 * Copyright (C) 2021-2022  Gioacchino Mazzurco <gio@eigenlab.org>
 * Copyright (C) 2021-2022  Asociación Civil Altermundi <info@altermundi.net>
 * 
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'package:retroshare_dart_wrapper/retroshare.dart';
import 'package:tier1_utils/tier1_env.dart';

void printPostSummary(dynamic meta)
{
  print('${meta["mMsgId"]} ${meta["mPublishTs"]["xstr64"]} ${meta["mMsgName"]}');
}

int main(List<String> arguments)
{
  if(arguments.isEmpty)
  {
    dbg('Needs <forum id> [<post id>...]');
    return -1;
  }

  var forumId = arguments[0];

  tier1_env_setup();

  if(arguments.length > 1)
  {
    RsGxsForum.getForumContent(forumId, arguments.sublist(1)).then(
      (posts) => posts.forEach((post) { printPostSummary(post['mMeta']); }) );
      return 0;
  }

  RsGxsForum.getForumMsgMetaData(forumId).then(
    (metas) => metas.forEach((meta) { printPostSummary(meta); }) );
  return 0;
}
