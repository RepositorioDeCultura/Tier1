#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
elRepo.io

Copyright (C) 2020  Gioacchino Mazzurco <gio@eigenlab.org>
Copyright (C) 2020  Asociación Civil Altermundi <info@altermundi.net>

SPDX-License-Identifier: AGPL-3.0-only
"""

import sys
import retroshareJsonApi as rs

def downloadLink(url):
	mResp = rs.jsonApiCall(
		"/rsFiles/parseFilesLink", { "link": url } )
	if(mResp["retval"]["errorNumber"] != 0):
		print(url.encode("utf8"), mResp["retval"])
		return mResp["retval"]

	return rs.jsonApiCall( "/rsFiles/requestFiles",
						   { "collection": mResp["collection"] } )["retval"]


def main():
	argIt = iter(sys.argv) ; next(argIt) # Skip script name in argv
	for fUrl in argIt: print(downloadLink(fUrl))

if __name__ == '__main__':
    main()
