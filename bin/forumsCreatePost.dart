/*
 * elRepo.io
 * 
 * Copyright (C) 2021-2022  Gioacchino Mazzurco <gio@eigenlab.org>
 * Copyright (C) 2021-2022  Asociación Civil Altermundi <info@altermundi.net>
 * 
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'package:retroshare_dart_wrapper/retroshare.dart';
import 'package:tier1_utils/tier1_env.dart';

int main(List<String> arguments)
{
  if(arguments.length != 4)
  {
    dbg('Needs <forum id> <author id> <post title> <post body>');
    return -1;
  }

  var forumId = arguments[0];
  var authorId = arguments[1];
  var title = arguments[2];
  var mBody = arguments[3];

  tier1_env_setup();
  RsGxsForum.createPost(forumId, title, mBody, authorId).then(
    (postId) => print(postId) );
  return 0;
}
