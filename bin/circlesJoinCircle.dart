/*
 * elRepo.io
 * 
 * Copyright (C) 2021-2022  Gioacchino Mazzurco <gio@eigenlab.org>
 * Copyright (C) 2021-2022  Asociación Civil Altermundi <info@altermundi.net>
 * 
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'package:retroshare_dart_wrapper/retroshare.dart';
import 'package:tier1_utils/tier1_env.dart';

int main(List<String> arguments)
{
  if(arguments.length != 2)
  {
    dbg('Circle id and own identity id are required');
    return -1;
  }

  var circleId = arguments[0];
  var ownId = arguments[1];

  tier1_env_setup();
  var retval = false;
  RsGxsCircles.requestCircleMembership(circleId, ownId).
    then((ret) => retval = ret);

  return retval?0:-1;
}
