#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
elRepo.io

Copyright (C) 2020  Gioacchino Mazzurco <gio@eigenlab.org>
Copyright (C) 2020  Asociación Civil Altermundi <info@altermundi.net>

SPDX-License-Identifier: AGPL-3.0-only
"""

import retroshareJsonApi as rs
from rsgxsforums import *

def main():
	mResp = rs.jsonApiCall("/rsGxsForums/getForumsSummaries")
	if(not mResp["retval"]):
		print( "Failed forum summareis retrieval `retval`", mResp["retval"] )
		exit(-3)

	for forumMeta in mResp["forums"]:
		if(forumMeta["mSubscribeFlags"] & GXS_GROUP_SUBSCRIBE_SUBSCRIBED):
			printForumSummary(forumMeta)

if __name__ == '__main__':
    main()
