#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
elRepo.io

Copyright (C) 2020-2021  Gioacchino Mazzurco <gio@eigenlab.org>
Copyright (C) 2020-2021  Asociación Civil Altermundi <info@altermundi.net>

SPDX-License-Identifier: AGPL-3.0-only
"""

from enum import IntEnum
import retroshareJsonApi as rs

class RsGxsCircleEventCode(IntEnum):
	UNKNOWN                                        = 0x00,
	CIRCLE_MEMBERSHIP_REQUEST                      = 0x01,
	CIRCLE_MEMBERSHIP_ID_ADDED_TO_INVITEE_LIST     = 0x02,
	CIRCLE_MEMBERSHIP_LEAVE                        = 0x03,
	CIRCLE_MEMBERSHIP_ID_REMOVED_FROM_INVITEE_LIST = 0x04,
	NEW_CIRCLE                                     = 0x05,
	CACHE_DATA_UPDATED                             = 0x06,
	CIRCLE_DELETED                                 = 0x07,
	CIRCLE_UPDATED                                 = 0x08,
