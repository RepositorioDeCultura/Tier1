/*
 * elRepo.io
 * 
 * Copyright (C) 2021-2022  Gioacchino Mazzurco <gio@eigenlab.org>
 * Copyright (C) 2021-2022  Asociación Civil Altermundi <info@altermundi.net>
 * 
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'package:retroshare_dart_wrapper/retroshare.dart';
import 'package:tier1_utils/tier1_env.dart';

int main(List<String> arguments)
{
  if(arguments.isEmpty)
  {
    dbg('Identity name is required');
    return -1;
  }

  var identityName = arguments[0];
  var pgpPass = authPassphrase;

  if(arguments.length == 2)
  {
    pgpPass = arguments[1];
  }

  tier1_env_setup();
  RsIdentity.createIdentity(
    identityName, pseudonimous: false, pgpPassword: pgpPass ).
  then((id) => print(id));

  return 0;
}
