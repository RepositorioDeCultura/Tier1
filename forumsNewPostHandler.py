#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
elRepo.io

Copyright (C) 2020  Gioacchino Mazzurco <gio@eigenlab.org>
Copyright (C) 2020  Asociación Civil Altermundi <info@altermundi.net>

SPDX-License-Identifier: AGPL-3.0-only
"""

import retroshareJsonApi as rs
import filesDownloadLinks as rsFiles
import forumsMarkMessageRead as rsForums
import re, sys

URL_PATTERN = re.compile('[A-Za-z+-]+://[^"<>\^`{|}\s]+')

def handlePost(forumId, postId):
	mResp = rs.jsonApiCall(
				"/rsGxsForums/getForumContent",
				{
					"forumId": forumId,
					"msgsIds": [ postId ]
				} )

	if(not mResp["retval"]):
		print(
			"Failed message data retrieval `forumId`, `msgsIds`, `retval`",
			forumId, postId, mResp["retval"] )

	for mMsg in mResp["msgs"]:
		for sUrl in re.findall(URL_PATTERN, mMsg["mMsg"]):
			rsFiles.downloadLink(sUrl)
		
		rsForums.setMessageRead(forumId, postId, True)


def main():
	forumId = sys.argv[1]
	postId  = sys.argv[2]
	handlePost(forumId, postId)


if __name__ == '__main__':
	main()
