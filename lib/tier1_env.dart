/*
 * elRepo.io
 * 
 * Copyright (C) 2021-2022  Gioacchino Mazzurco <gio@eigenlab.org>
 * Copyright (C) 2021-2022  Asociación Civil Altermundi <info@altermundi.net>
 * 
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'dart:io' show Platform;
import 'package:retroshare_dart_wrapper/retroshare.dart';

void tier1_env_setup()
{
  var env = Platform.environment;
  var apiUrl = env['RS_API_URL'] ?? 'http://127.0.0.1:9092/';
  var apiUser = env['RS_API_USER'] ?? 'buryTheDead';
  var apiPass = env['RS_API_PASS'] ?? 'inNodeCementery';

  setRetroshareServicePrefix(apiUrl);
  initRetroshare(
        locationId:'', identityId:'',
        passphrase: apiPass, apiUser: apiUser );
}