/*
 * elRepo.io
 * 
 * Copyright (C) 2021-2022  Gioacchino Mazzurco <gio@eigenlab.org>
 * Copyright (C) 2021-2022  Asociación Civil Altermundi <info@altermundi.net>
 * 
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'package:retroshare_dart_wrapper/retroshare.dart';
import 'package:tier1_utils/tier1_env.dart';

int main(List<String> arguments)
{
  if(arguments.length != 2)
  {
    dbg('Needs <Maximum download rate in KB/s> <Maximum upload rate in KB/s>');
    return -1;
  }

  var maxDownload = int.parse(arguments[0]);
  var maxUpload = int.parse(arguments[1]);

  tier1_env_setup();
  RsConfig.setMaxDataRates(maxDownload, maxUpload);
  return 0;
}
