#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
elRepo.io

Copyright (C) 2020-2021  Gioacchino Mazzurco <gio@eigenlab.org>
Copyright (C) 2020-2021  Asociación Civil Altermundi <info@altermundi.net>

SPDX-License-Identifier: AGPL-3.0-only
"""

import retroshareJsonApi as rs
import forumsSubscribe as rsForums
import sys

"""You are not subscribed"""
GXS_GROUP_SUBSCRIBE_NOT_SUBSCRIBED = 0x08

def handleForum(forumId):
	mResp = rs.jsonApiCall(
		"/rsGxsForums/getForumsInfo", {"forumIds":[forumId]} )

	if(not mResp["retval"]):
		print( "Failed forum info retrieval `retval`", mResp["retval"] )
		return

	for forumInfo in mResp["forumsInfo"]:
		forumMeta = forumInfo["mMeta"]
		if(
			( forumMeta["mSubscribeFlags"]
				& GXS_GROUP_SUBSCRIBE_NOT_SUBSCRIBED )
			and forumMeta["mGroupName"].startswith("elRepo.io_") ):
			print(
				"Subscribing forum:",
				forumId, forumMeta["mGroupName"].encode("utf8") )
			rsForums.subscribeForum(forumId, True)
			rsForums.setStorageTime(forumId, rsForums.FORUM_MAX_STORAGE_TIME)
			rsForums.setSyncTime(forumId, rsForums.FORUM_MAX_SYNC_TIME)

def main():
	forumId = sys.argv[1]
	handleForum(forumId)

if __name__ == '__main__':
	main()
