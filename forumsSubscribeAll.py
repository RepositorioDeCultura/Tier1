#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
elRepo.io

Copyright (C) 2020  Gioacchino Mazzurco <gio@eigenlab.org>
Copyright (C) 2020  Asociación Civil Altermundi <info@altermundi.net>

SPDX-License-Identifier: AGPL-3.0-only
"""

import json, requests, time, math

JSON_API_URL = "http://127.0.0.1:9092/"
JSON_API_USER = "buryTheDead"
JSON_API_PASSWORD = "inNodeCementery"

def jsonApiCall(
	function, params = None,
	baseUrl = JSON_API_URL,
	apiUser = JSON_API_USER, apiPassword = JSON_API_PASSWORD ):
	return requests.post(
		url = baseUrl + function,
		json = params,
		auth = ( apiUser, apiPassword ) ).json()


""" You have the admin key for this group """
GXS_GROUP_SUBSCRIBE_ADMIN = 0x01

"""
You have the publish key for thiss group. Typical use: publish key in
channels are shared with specific friends.
"""
GXS_GROUP_SUBSCRIBE_PUBLISH = 0x02

"""
you are subscribed to a group, which makes you a source for this group to your
friend nodes.
"""
GXS_GROUP_SUBSCRIBE_SUBSCRIBED = 0x04

"""You are not subscribed"""
GXS_GROUP_SUBSCRIBE_NOT_SUBSCRIBED = 0x08

def main():
	mResp = jsonApiCall("/rsGxsForums/getForumsSummaries")
	if(not mResp["retval"]):
		print( "Failed forum summareis retrieval `retval`", mResp["retval"] )
		exit(-3)

	for forumMeta in mResp["forums"]:
		if(forumMeta["mSubscribeFlags"] & GXS_GROUP_SUBSCRIBE_NOT_SUBSCRIBED):
			print(
				"Subscribing forum:",
				forumMeta["mGroupId"], forumMeta["mGroupName"].encode("utf8") )
			jsonApiCall(
				"/rsGxsForums/subscribeToForum",
				{ "forumId": forumMeta["mGroupId"], "subscribe": True } )

if __name__ == '__main__':
    main()
