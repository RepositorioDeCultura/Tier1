#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
elRepo.io

Copyright (C) 2020  Gioacchino Mazzurco <gio@eigenlab.org>
Copyright (C) 2020  Asociación Civil Altermundi <info@altermundi.net>

SPDX-License-Identifier: AGPL-3.0-only
"""

"""RetroShare circles event type"""
RsEventType_GXS_CIRCLES = 8

"""RetroShare forum event type"""
RsEventType_GXS_FORUMS = 10
